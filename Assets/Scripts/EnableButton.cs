﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EnableButton : MonoBehaviour {

	// Use this for initialization
	void Start () {
        for(int  i =1; i< LevelManager.level; i++)
        {
            GameObject gameObject = GameObject.FindGameObjectWithTag("Level" + i);
            Button b = gameObject.GetComponent<Button>();
            b.interactable = enabled;
        }
        Invoke("EnableLevelButton", 2f);
    }

    // Update is called once per frame
    void Update () {
		
	}
    public void EnableLevelButton()
    {
        GameObject gameObject = GameObject.FindGameObjectWithTag("Level" + LevelManager.level);
        Button b = gameObject.GetComponent<Button>();
        b.interactable = enabled;
  
    }
}
