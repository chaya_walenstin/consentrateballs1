﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateLittleBalls : MonoBehaviour {
    public Sprite[] hindrances;
    public Transform right;
    public Transform left;
    public Camera mainCamera;
    float cameraHeight;
    public GameObject Circle;
    public int hindrancesAmount = 5;
    public int minTimeToAddHind = 3;
    public int maxTimeToAddHind = 6;
    public float hindrancesSize = 0.85f;

    public List<GameObject> Circlelist;

    // Use this for initialization
    void Start () {
        float ratio = Screen.width / (float)Screen.height;
        cameraHeight = mainCamera.orthographicSize;//מקבל חצי מהגובה
        float cameraWidth = cameraHeight * ratio;
        float halfWidth = cameraWidth / 2;
        left.position = mainCamera.transform.position - new Vector3(halfWidth, 0, 0);
        right.position = mainCamera.transform.position + new Vector3(halfWidth, 0, 0);
        float timeToAddHindrances = Random.Range(minTimeToAddHind, maxTimeToAddHind);
        float timeToRepeatHindraces = Random.Range(minTimeToAddHind, maxTimeToAddHind);
        Invoke("Hindrances", timeToAddHindrances);
    }

    public void CreateHindrances(int startNum, int num)
    {

        for (int i = startNum; i < num + startNum; i++)
        {

            Vector3 v = new Vector3(Random.Range(left.position.x, right.position.x), Random.Range(cameraHeight * (-1 / 2), cameraHeight));
            GameObject circleClone = Instantiate(Circle, v, Quaternion.identity);
            circleClone.transform.localScale = new Vector3(hindrancesSize, hindrancesSize, 1);
            int randomsprite = Random.Range(0, hindrances.Length);        
            circleClone.transform.GetComponent<SpriteRenderer>().sprite = hindrances[randomsprite];
            circleClone.transform.GetChild(0).GetComponent<SpriteRenderer>().enabled = false;          
            circleClone.name = i.ToString();
            circleClone.tag = randomsprite.ToString();
            Circlelist.Add(circleClone);

        }
        //numberOfFirstBalls = numberOfBallsToCreate;

    }
    public void Hindrances()
    {
        int hindAmount = Random.Range(3, hindrancesAmount-1);
        for(int i = 0; i< hindAmount; i++)
        {
            CreateHindrances(0, i);
        }
       
        
    }
    public void CleanHindances()
    {
        foreach (var item in Circlelist)
        {
            Destroy(item);
        }
        Circlelist.Clear();
        float timeToAddHindrances = Random.Range(minTimeToAddHind, maxTimeToAddHind);
        Invoke("Hindrances", timeToAddHindrances);
    }
    // Update is called once per frame
    void Update () {
		
	}
}
