﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SetScore : MonoBehaviour
{
    public int numberOfGames = 3;
    private static int currentGame = 1;
    private static int score = 0;
    public Text scoreText;
    private GameObject scoreObject;
    private GameObject createBallsObject;
    public Text currentGameText;
    private bool successed;
    public Text gameOver;    // Use this for initialization
    void Start()
    {
        scoreObject = GetComponent<GameObject>();
        GameObject[] objects = GameObject.FindGameObjectsWithTag("createBalls");
        createBallsObject = objects[0];
    }

    // Update is called once per frame


    internal void UpdateScore(bool isSuccess)
    {
        successed = isSuccess;
        Invoke("UpdateUI", 1.0f);


    }

    void UpdateUI()
    {

        // Debug.Log("updateScore");
        currentGame += 1;
        currentGameText.text = currentGame.ToString() + "/" + numberOfGames.ToString();
        PlayerPrefs.SetInt("you", score * 20);
        if (PlayerPrefs.GetInt("cilmax") < PlayerPrefs.GetInt("you"))
        {
            print("cilmax" + PlayerPrefs.GetInt("you"));
            PlayerPrefs.SetInt("cilmax", PlayerPrefs.GetInt("you"));
           
        }
        if (successed)
        {
            score += 1;
            currentGame++;
            
            //show ui
        }
        //score < (currentGame / 3) &&

        if (score < (currentGame / 3) && currentGame > 6)
        {

            SceneManager.LoadScene("GameOver");
            //end game

        }

        if (currentGame == numberOfGames)
        {
      
            //end game
           
        }
        //else if (currentGame - score < currentGame / 3)
        //{
        //   // restart game
        //}
        else
        {
            Debug.Log("CreateCirclesDynamic");
            createBallsObject.GetComponent<CreateCirclesDynamic>().NewLevel(currentGame);
            //new level
        }
        scoreText.text = "Score: " + score.ToString() + "/" + (currentGame - 1).ToString();
    }


}
