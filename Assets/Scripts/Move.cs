﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour
{
    public float minX = -7;
    public float maxX = 7;
    public float minY = -7;
    public float maxY = 7;
    float timer;
    public float minTimer = 10;
    public float maxTimer = 12;
    float currentTime;
    private Rigidbody2D r;

    // Use this for initialization
    void Start()
    {

        r = GetComponent<Rigidbody2D>();
        currentTime = Random.Range(minTimer, maxTimer);

    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        if (timer >= currentTime)
        {
            timer = 0;
            currentTime = Random.Range(minTimer, maxTimer);
            moveCircle();

        }
    }
    void moveCircle()
    {
        float randomX = Random.Range(minX, maxX);
        // Debug.Log("randomX-" + randomX);
        float randomY = Random.Range(minY, maxY);
        // Debug.Log("randomY-" + randomY);
        r.velocity = new Vector2(randomX, randomY);
        float torque = Random.Range(0, 100);
        if (torque > 50)
            r.AddTorque(Random.Range(torque/2, 80), ForceMode2D.Force);
    }
}
