﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sprites : MonoBehaviour
{
    public Sprite spriteBlue1;
    public Sprite spriteBlue2;
    public Sprite spriteBlue3;
    public Sprite spriteBlue4;
    public Sprite spriteBlue5;
    public Sprite spriteBlue6;
    public Sprite spriteRed1;
    public Sprite spriteRed2;
    public Sprite spriteRed3;
    public Sprite spriteRed4;
    public Sprite spriteRed5;
    public Sprite spriteRed6;
    public Sprite spriteGreen1;
    public Sprite spriteGreen2;
    public Sprite spriteGreen3;
    public Sprite spriteGreen4;
    public Sprite spriteGreen5;
    public Sprite spriteGreen6;
    public Sprite spriteOrange1;
    public Sprite spriteOrange2;
    public Sprite spriteOrange3;
    public Sprite spriteOrange4;
    public Sprite spriteOrange5;
    public Sprite spriteOrange6;
    public Sprite[] nullSprite;
    public Sprite[] circleSprites;
  
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
