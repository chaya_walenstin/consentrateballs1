﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OnBallClick : MonoBehaviour
{
    private GameObject scoreObject;
    private GameObject ball;
    private GameObject createBallsObject;
    public Sprite happy;
    public Sprite sad;
    public Sprite[] nullSprite;
    public Sprite blueSprite;
    public Sprite redSprite;
    public Sprite greenSprite;
    public Sprite orangeSprite;
    //private BoxCollider2D ballBoxCollider;
    private static int numberOfClickBalls = 0;
    public AudioSource myAudio;
    public AudioClip audioclipe;
    public Animator animators;
    public Animator animatorRed;
    public Animator animatorBlue;
    public Animator animatorGreen;
    public Animator animatorOrange;
    public static int successGames = 0;

    // Use this for initialization
    void Start()
    {
        
        ball = gameObject;
        GameObject[] objects = GameObject.FindGameObjectsWithTag("score");
        scoreObject = objects[0];

        objects = GameObject.FindGameObjectsWithTag("createBalls");
        createBallsObject = objects[0];
        nullSprite = new Sprite[] { blueSprite, redSprite, greenSprite, orangeSprite };
        myAudio = GetComponent<AudioSource>();
        print(myAudio);
        animators = GetComponent<Animator>();
         // myAudio = GetComponent<AudioSource>();
        //ballBoxCollider = GetComponent<BoxCollider2D>();
    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnMouseDown()
    {
        if (ball.layer==10)
        {
            //Debug.Log("OnMouseDown");
            numberOfClickBalls += 1;
            int numberOfFirstBalls = createBallsObject.GetComponent<CreateCirclesDynamic>().numberOfFirstBalls;
            bool isSuccess = (int.Parse(ball.name) < numberOfFirstBalls);
            //ball.transform.GetChild(0).GetComponent<SpriteRenderer>().enabled = true;
            ball.transform.GetComponent<Transform>().eulerAngles = new Vector3(0, 0, 0);
            createBallsObject.GetComponent<CreateCirclesDynamic>().showLeftBalls(-1 * numberOfClickBalls);
            int index = 0;
            if (int.Parse(ball.tag) < 3)
            {
                index = 0;
            }
            if (int.Parse(ball.tag) >= 3 && int.Parse(ball.tag) < 6)
            {
                index = 1;
            }
            if (int.Parse(ball.tag) >= 6 && int.Parse(ball.tag) < 9)
            {
                index = 2;
            }
            if (int.Parse(ball.tag) >= 9)
            {
                index = 3;
            }
            ball.GetComponent<SpriteRenderer>().sprite = nullSprite[index];
            if (isSuccess)
            {

                // ball.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = happy;
                //ball.transform.GetChild(0).GetComponent<Transform>().localScale.x = 2;
              
                animators.GetComponent<Animator>().enabled = true;
               
            }
            else
            {
                myAudio.enabled = true;
                //ball.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = sad;

            }

            //Debug.Log("OnMouseDown" + ball.name);
            //ballBoxCollider.enabled = false;
            ball.transform.GetComponent<CircleCollider2D>().enabled = false;
            if (numberOfClickBalls == numberOfFirstBalls || !isSuccess)
            {
                if (numberOfClickBalls == numberOfFirstBalls)
                {

                    successGames++;
                    if (successGames == 3)
                    {
                        successGames = 0;
                        SceneManager.LoadScene(LevelManager.menu);
                    }
                }
                // ball.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
                foreach (var item in createBallsObject.GetComponent<CreateCirclesDynamic>().Circlelist)
                {
                    item.GetComponent<CircleCollider2D>().enabled = false;
                }
                numberOfClickBalls = 0;
                scoreObject.transform.GetComponent<SetScore>().UpdateScore(isSuccess);
                
            }
        }



    }
}
