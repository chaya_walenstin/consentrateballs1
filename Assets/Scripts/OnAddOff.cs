﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnAddOff : MonoBehaviour
{
    public AudioSource myAudio;
    bool ISenabled = false;
    // Use this for initialization
    void Start()
    {
        myAudio = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
    }
    private void OnMouseDown()
    {
        myAudio.enabled = !ISenabled;
    }
}
