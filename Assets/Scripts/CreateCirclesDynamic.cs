﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class CreateCirclesDynamic : MonoBehaviour
{
    private GameObject G;
    public GameObject Circle;
    private Sprite[] circleSprites;
    private Color[] colors;
    private GameObject[] circles;
    public Transform right;
    public Transform left;
    public Camera mainCamera;
    public int numberOfBallsToCreate = 0;
    public int numberOfFirstBalls = 0;
    public List<GameObject> Circlelist;
    float cameraHeight;
    public Text ballLeftText;
    public Sprites BallsSprites;
    public Sprite spriteWhite;
    public int level = 1;
    private int firstBalls = 2;
    private int lastBalls = 3;
    public int timeToAddBalls = 4;
    public int addBalls = 0;
    public int repeatCreateBalls = 1;
    static int ballsCreated = 0;
    public GameObject createHinrances;
    public float circleSize = 1;
    public float closePrecent = 0;

    // Use this for initialization
    void Start()
    {
        BallsSprites = GetComponent<Sprites>();
        circleSprites = new Sprite[]
        {
            BallsSprites.spriteBlue1, BallsSprites.spriteBlue2, BallsSprites.spriteBlue3,
            BallsSprites.spriteRed1, BallsSprites.spriteRed2, BallsSprites.spriteRed3,
            BallsSprites.spriteGreen1, BallsSprites.spriteGreen2, BallsSprites.spriteGreen3,
            BallsSprites.spriteOrange1, BallsSprites.spriteOrange2, BallsSprites.spriteOrange3
        };
        PlayerPrefs.GetInt("you");
        PlayerPrefs.GetInt("climax");
        PlayerPrefs.DeleteKey("you");
        //  G = GetComponent<GameObject>();
        float ratio = Screen.width / (float)Screen.height;
        cameraHeight = mainCamera.orthographicSize;//מקבל חצי מהגובה
        float cameraWidth = cameraHeight * ratio;
        float halfWidth = cameraWidth / 2;
        left.position = mainCamera.transform.position - new Vector3(halfWidth, 0, 0);
        right.position = mainCamera.transform.position + new Vector3(halfWidth, 0, 0);
        NewLevel(level);
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void CreateCircles(int startNum, int num)
    {

        for (int i = startNum; i < num + startNum; i++)
        {
            
            Vector3 v = new Vector3(Random.Range(left.position.x, right.position.x), Random.Range(cameraHeight * (-1/2), cameraHeight * (1 / 2)));
            GameObject circleClone = Instantiate(Circle, v, Quaternion.identity);
            circleClone.transform.localScale = new Vector3(circleSize, circleSize, 0);
            int randomsprite = Random.Range(0, circleSprites.Length);
            //Debug.Log("randomsprite " + randomsprite);
            circleClone.transform.GetComponent<SpriteRenderer>().sprite = circleSprites[randomsprite];
            circleClone.transform.GetChild(0).GetComponent<SpriteRenderer>().enabled = false;
            // circleClone.transform.GetComponent<SpriteRenderer>().color = colors[Random.Range(0, colors.Length)];
            circleClone.name = i.ToString();

            circleClone.tag = randomsprite.ToString();
            //Debug.Log("tag" + circleClone.tag);
            Circlelist.Add(circleClone);

            //  Debug.Log("name" + circleClone.name);



        }
        //numberOfFirstBalls = numberOfBallsToCreate;

    }
    public void CreateCircleAfterTime()
    {
        numberOfBallsToCreate = Random.Range(lastBalls, lastBalls + addBalls);
        CreateCircles(numberOfFirstBalls, numberOfBallsToCreate);
        Invoke("ColorWhite", level*3f);
        ballsCreated++;
    }
    public void NewLevel(int Level)
    {
        // Debug.Log("Level " + Level);
        if (Level < 30)
        {
            firstBalls = 2 + Level / 8;
            // Debug.Log("Level/8 " + Level / 8);
            lastBalls = 2 + Level / 8;
        }
        else
        {
            firstBalls = 6;
            lastBalls = 6;
        }

        timeToAddBalls = 4 - Level / 8;
        // Debug.Log("new level");
        DeleteBalls();
        numberOfBallsToCreate = Random.Range(firstBalls, firstBalls + 1);
      
        numberOfFirstBalls = numberOfBallsToCreate;
        ballLeftText.enabled = false;
        CreateCircles(0, numberOfBallsToCreate);
        for(ballsCreated =0; ballsCreated<repeatCreateBalls; ballsCreated++)
            Invoke("CreateCircleAfterTime", timeToAddBalls+ballsCreated);
            
        
    }

    public void ColorWhite()
    {
        foreach (var item in Circlelist)
        {
            int nullSpriteIndex = 0;
            int nullSpritesAmount = BallsSprites.nullSprite.Length;
            if(nullSpritesAmount == 1)
            {
                nullSpriteIndex = 0;
            }
            else
            {
                nullSpriteIndex = int.Parse(item.tag);
            }
            item.transform.GetComponent<SpriteRenderer>().sprite = BallsSprites.nullSprite[nullSpriteIndex];
            item.layer = 10;
            item.transform.localScale = new Vector3(circleSize-closePrecent, circleSize-closePrecent, 1);
            item.GetComponent<Move>().enabled = false;
            // Destroy(item.GetComponent<Rigidbody2D>());
            item.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
            item.GetComponent<Rigidbody2D>().angularVelocity = 0;
            

        }
        showLeftBalls(0);


    }
    public void DeleteBalls()
    {
        foreach (var item in Circlelist)
        {
            Destroy(item);
        }
        Circlelist.Clear();
        if (createHinrances)
            createHinrances.GetComponent<CreateLittleBalls>().CleanHindances();
    }
    public void showLeftBalls(int num)
    {
        ballLeftText.enabled = true;
        ballLeftText.text = "Left Balls: " + (numberOfFirstBalls + num).ToString();
    }
}
