﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseManager : MonoBehaviour {
    public bool paused;
    public Button btnPause;
    public Sprite pauseSprite;
    public Sprite playSprite;

    // Use this for initialization
    void Start () {
        paused = false;

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void changeStatus()
    {
        paused = !paused;
        if(paused)
        {
            btnPause.GetComponent<Image>().sprite = pauseSprite;
        }
        else
        {
            btnPause.GetComponent<Image>().sprite = playSprite;
        }
        if (paused)
        {
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = 1;
        }
    }
}
